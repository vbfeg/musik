Datum;Lied;Team
;Allianz - GODIE im Haus der Begegnung;
15.01.17;"Du bist gut, Herr
Meine Zuflucht und Stärke
Vater, führe uns in deine Nähe
Wir schaun auf dich
Treu
Blessed be your name
Du bist meine Zufluchtsort
Wege vor mir";sk
22.01.17;"Du bist schöpfer (Siehst Du die mächtigen Berge)
wir sind hier zusammen in jesu namen
all die fülle ist in dir oh herr
dank sei dir ja dank sei dir wir danken dir herr
god is great and his praise fills the earth
licht dieser welt
when the music fades
der könig lebt - jesus christus starb als könig (in the mix)";vb
29.01.17;"Darum jubel ich dir zu 
Alle Schöpfung staunt und preist
Wir schaun auf dich 
Großer, herrlicher Gott
Allein deine Gnade genügt
Keiner ist wie du
Du hast Erbarmen
Meine Gnade weicht nicht von dir";sk
05.02.17;"Du bist mächtig
(1) Du bist der einzig wahre Gott  - 
Mighty to save
Jesus, in deinem Namen ist die Kraft
Du bist größer
Dein Segen umhülle uns";eg
12.02.17;"Jesus berühre mich (nur den saum deines gewandes)
Von ganzem Herzen bete ich an ( i will worship with all of my heart)
the splendour of a king
Die ganze Schöpfung jubelt dir zu (halleluja, du bist mächtig du bist gut)
your love never fails
Über alle Welt bist du der Herr
nur den Saum deines gewandes
Lass mir das Ziel vor Augen bleiben";vb
19.02.17;"Gut, dass wir einander haben
Herr in deinem Namen 
Ein Leben gegeben / Ein Leben für Gott
Wir sind Gottes Kirche
Herz und Herz vereint zusammen
(1)  God of this city
Vater, segne uns ";sk
26.02.17;"bete gott hört/wenn gott schweigt und wir reden
ich will dir danken herr (auf grüne wiesen hast du mich gebracht)
leuchtturm 
wir sind gottes kirche
over the mountains and the sea
lobpreis und ehre
Du bist der Schöpfer des Universums
dass wir in dir bleiben
what if his people prayed";vb
05.03.17;"Ich will dir danken, Herr
Großer herrlicher Gott
So groß ist der Herr
Blessed be your name
Herrlicher Gott  / Du bist der einzig wahre Gott
All die Fülle
Geh unter der Gnade";sk
12.03.17;"In Gottes Haus sind off´ne Türen (Kids-Lied)
Anker in der Zeit
Zehntausend Gründe
Lobe den Herren, den mächtigen KÖnig der Ehren
Groß und wunderbar 
(1) TAKE MY LIFE (holiness)
No longer a slave (NEU)
Der Herr möge dich segnen";eg
19.03.17;"Zeichen Deiner Liebe (Vater ich will dir danke)
Ich singe Dir ein Liebeslied
Auf, Seele, Gott zu loben
All die Fülle ist in dir oh herr
Dank sei dir ja dank sei dir
Du, du bist gott
Oh our lord and king
(2) Take my life (holiness)";vb
26.03.17;"Danke für diesen guten Morgen
(1) Spar dir deine Sorgen (Kinderlied - NEU)
Herr, du durchschaust und kennst mich
Welch ein Freund ist unser Jesus
Du allein kennst meine Gedanken
Vater, ich komme jetzt zu dir
Lege deine Sorgen nieder
Berge mich in deinem Arm";eg
02.04.17;"Du bist der Schöpfer des Universums
Halleluja / Die ganze Schöpfung jubelt dir zu
Vater, wir sind hier / Wir beten dich an 
Heilig heilig das Lamm Gottes
Keiner ist wie du / There is none like you
Our God 
Immer bist du, Herr, bei mir / Herr, du durchschaust und kennst mich
Bis ans Ende der Welt";sk
09.04.17;"Blessed is the king who comes 
Darum jubel ich dir zu
Vater, ich komme jetzt zu dir
Du bist treu, Herr
Allein, deine Gnade genügt
Sei still in der Gegenwart des Herrn
Herr, ich suche deine Ruhe / Starker Turm
Herr, wir bitten, komm und segne uns";sk
"Karfreitag
14.04.2017";"Above all
Jesus, Erlöser der Welt
Jesus, Herr, ich denke an dein Opfer
Zwischen Himmel und Erde
Komm zu Jesus";sk
"Ostersonntag
16.04.2017";"Stellt euch vor, es wäre wahr (Kids - Lied)
Feiert Jesus
Herr, dein Name sei erhöht (D + Engl.)
Oh happy day
ER ist Herr
Wir brauchen Jesus nicht bei den Toten suchen (Kids - Lied)";eg
;"Gemeindefreizeit auf dem Forggenhof
Neue Lieder dort: 
Königskind
Bis ich dir gegenübersteh/ Till I see You
Freizeitlied: Zwischen Himmel und Erde";
30.04.17;;EG
07.05.17;"Du bist gut, Herr
Alle Schöpfung staunt und preist
Es ist so gut wieder vor dir zu stehen / Nichts will ich mehr
Großer herrlicher Gott
Der Einzige
Gott ist gegenwärtig
Der Herr segne dich";sk
14.05.17;"danke für die sonne
alles hast du wunderbar gemacht (gut zu uns)
lege deine sorgen nieder
lobe den herren den mächtigen könig 
oh herr gieße ströme des lebendigen Wassers aus
dein ist die herrschaft
Let Everything That Has Breath
history maker
oh I could sing unending songs (happy song)
joy in the storm";vb
21.05.17;"Danke
Keiner ist wie du / There is none like you
Schöpfer aller Himmel
10.000 Reasons
Auge im Sturm / Starker Turm
Herr, wohin sonst
Herr, unendlich viel 
Ich bin bei dir";sk
28.05.17;"welch ein freund ist unser jesus
Du bist schöpfer (Siehst Du die mächtigen Berge)
du, du bist gott
lobe den herren den mächtigen könig der ehren 
du bist erhoben für immer gehört dir der thron
singt ein lied von gott (gott ist da gott ist da)
seid nicht bekümmert
don’t you know i ve always loved you (third day)";vb
"04.06.2017
Pfingsten";"Danke für diesen guten Morgen
Das ist die Freiheit der Kinder Gottes
Rückenwind / Du bist der Herr, der mein Haupt erhebt
Tief in mir hab ich Sehnsucht nach dir
...";sk
?! Bitte mit Leitung absprechen ob eine Gebetszeit sein soll und wo die hinkommt/ wer die Initiiert (wir wollte das doch wieder aufnehmen, oder?!) (vb);;
11.06.17;"Jesus Christus ist aller Herr
Wenn ich dran denke
I believe in Jesus
Jesus du bist wunderbar
Jesus, du allein bist genug
Jesus, sei das Zentrum
Mittelpunkt
Anker in der Zeit";eg
18.06.17;"Auf Seele, Gott zu loben
Ich bin auf dem Weg in die Herrlichkeit (Kinderlied)
Ich will dir danken, Herr
Ich bin ein Königskind
Day by day
Ich bin entschieden zu folgen Jesus
Ja ich glaub an dich// Ich glaub an Gott den Vater
(1)  Into Your hand (NEU) // Jesus, I believe in You
Keine Macht der Welt";eg
25.06.17;Godie nach JOEL & Moni´s Hochzeit - , musik.gestaltet von Brands;
02.07.17;"Zeichen deiner Liebe
Wir schaun auf dich
Wo ich auch stehe
Mittelpunkt
Mein Jesus, mein Retter / My Jesus, my saviour
Jesus, sei das Zentrum
Anker in der Zeit
Meine Zuflucht und Stärke";sk
09.07.17;"Better is one day in Your courts
So sind deine Worte/ Wie auf dunklem Weg ein Licht
Alle Schöpfung staunt und preist
LESUNG Psalm
Du bist der einzig wahre Gott
Jeden Tag leb ich für dich, Herr
(2) Into Your hand/ Jesus, I believe in You
Ungeteiltes Herz
Der Herr möge dich segnen";eg
16.07.17;---- feg/citychurch -godi ---;
23.07.17;"Was für ein Mensch
Es ist so gut wieder vor dir zu stehen / Nichts will ich mehr
alles hast du wunderbar gemacht
(1) Im Herzen ein Lied
(1) Rock of ages (there is no rock, there is no god like our god)
We want to see jesus lifted high
Happy Day (the greatest day in history)
oh herr gieße ströme des lebendigen wassers aus";vb
30.07.17;"Seid nicht bekümmert
Mit meinem Gott kann ich über Mauern springen
Herr im Glanz Deiner Majestät
Du bist mächtig du bist heilig unbezwingbar in deiner kraft
danke für die sonne
(2) Im Herzen ein Lied
(2) Rock of ages (there is no rock, there is no god like our god)
oh our lord and king
holiness (take my life)";vb
06.08.17;"(kein Monatslied wg.Ferien)
Wiesen und Berge
Auge im Sturm
Du bist mein Zufluchtsort
Peacemaker
Berge mich in deinem Arm
Jesus, Herr, ich denke an dein Opfer
Der Herr segne dich";eg
13.08.17;"Danke
Jesus in my house
Jesus zu dir 
Du hast Erbarmen
Gnade und Wahrheit
Made to worship
Mein Jesus, mein Retter
Anker in der Zeit
Meine Gnade weicht nicht von dir";sk
20.08.17;"Ein neuer Tag beginnt und ich freu mich
Kinderlied - Lies die Bibel, bet jeden Tag
Vor dir kommt mein Herz zur Ruhe
So weit
10000 reasons
Vater, deine Liebe
Hope of the nations
Unser Herr sagt uns seinem Wort (Der Weinstock)";Anna Wesner
27.08.17;;sk
03.09.17;"Monatslied: Bis ich dir gegenübersteh
1) Komm und lobe den Herrn (10 000 Gründe)
2) Kinderlied: Dip Dip-Song
3) Du, du bist Gott (Hier bin ich, um dich zu finden)
4) Wo ich auch stehe, du warst schon da
5)There is none like You
6) Bis ich dir gegenübersteh (Til´I see You)
7) He is able / Er ist mächtig
8) Du starbst für uns auf Golgatha (Ich bin frei)
9) Wiederholung: Bis ich dir gegenübersteh (Monatslied)";eg
10.09.17;"alles hast du wunderbar gemacht (gut zu uns)
Till I see you / Bis ich dir gegenübersteh
Peacemaker
jesus christus starb für mich
(3) Im Herzen ein Lied
Mein Retter Erlöser
Du bist gut herr wahrhaft gut herr
wenn gott schweigt und wir reden / was betrübst du dich meine seele";vb
17.09.17;"Better is one day
Till I see you / Bis ich dir gegenüber steh
Lobe den Herren den mächtigen könig
dein ist die herrschaft und die stärke
Du bist der Schöpfer
Bewahre uns Gott";vb
24.09.17;"Wir sind Gottes Kirche 
Who am I
Mighty to save
(4) Bis ich dir gegenüber steh
Wir schaun auf dich
Gnade und Wahrheit
Geh unter der Gnade";sk
01.10.17;"Erntedank
Monatslied: Alles, was ich hab (Gut, gut, gut bist du Gott)
Zeichen deiner Liebe
Herr, ich sehe deine Welt
Unbeschreiblicher
Großer Gott, wir loben dich";eg
08.10.17;"Du bleibst an meiner Seite
Alles, was ich hab (Gut, gut gut bist du, Gott)
Wir danken dir Herr, denn du bist gut
Königskind
Du beschenkst (Du bist und bleibst mein Herr und Gott)
Du hast ERbarmen
Die Güte des Herrn hat kein Ende";eg
15.10.17;"(2) Alles, was ich hab
Herr, ich komme zu dir
Ein Leben für Gott
Alle Schöpfung
Herrlicher Gott
Vater segne uns";sk
22.10.17;"Blessed is the king who comes in the name of the lord
(3) Alles, was ich hab
Mein Erlöser lebt
my lighthouse
(1) I saw the light (crowder) https://www.youtube.com/watch?v=CQCk71sYxPs    
Happy song
Bist zu uns wie ein vater
joy in the storm";vb
29.10.17;;
05.11.17;"Lobpreis-Gottesdienst: 
Von ganzem Herzen
Made to worship
Großer Gott
Ich will dir danken, Herr
Du großer Gott
So groß ist der Herr
Monatslied: (1) Mutig komm ich vor den Thron 
(1) Gott und König
Der Einzige
Herrlicher Gott / Du bist der einzig wahre Gott
Größer
Wege vor mir";
12.11.17;"Jesus in meinem Haus / Jesus in my house
Kinderlied: Bärenstark
Mein Jesus, mein Retter / My Jesus, my saviour
Morgenstern
(2) Mutig komm ich vor den Thron
Gott ist gegenwärtig
Herr, wir bitten komm und segne uns";sk
19.11.17;"Gut, dass wir einander haben
Vater, wir sind hier
Keiner ist wie du
(3) Mutig komm ich vor den Thron
10.000 Gründe
Wohin sonst
Ich geb mein Leben hin an dich ";sk
26.11.17;"So bist du (wie der himmel)
Herr wohin sonst sollten wir gehen
hier bin ich 
Es ist so gut wieder vor dir zu stehen / Nichts will ich mehr
So lang ich bin will ich deinen namen preisen
bald schon kann es sein
Start a revolution in me";vb
03.12.17;"Advent ist ein Leuchten (NEU)
Freue dich Welt
Auf den Flügeln des Windes
Jesus, Erlöser der Welt/ Was für ein Mensch
Friedefürst, Wunderrat
Jesus, du bist wunderbar
Jesus, höchster Name
Ich bin bei dir";eg
10.12.17;"go tell it on the mountain
Jesus ist kommen Grund ewiger Freude
bahnt einen weg unserm gott
Blessed is the king who comes in the name of the lord
Freude bricht sich bahn
Lobpreis und ehre
Bald schon kann es sein
Hab keine angst und fürchte dich nicht";vb
17.12.17;"Advent ist ein Leuchten
Freude bricht sich Bahn / Wenn der König kommt
Herbei o ihr Gläubigen
Jesus, du bist wunderbar
Mein Jesus, mein Retter
The servant king
Ich steh an deiner Krippen hier
Bis ans Ende der Welt";sk
24.12.17;"Freue dich Welt
Jesus kam für dich
3 Kinderlieder während des Musicals
In der Nacht von Bethlehem
Fröhliche Weihnacht überall";eg
31.12.17;;sk
